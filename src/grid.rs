use std::iter::once;

pub trait Map<X, T, U> {
    type Output;
    fn map<F: Fn(X, T) -> U + Clone + 'static>(self, F) -> Self::Output;
}

pub trait Merge<Cogrid> {
    type Merger;
    fn merge(self, Cogrid) -> Self::Merger;
}

pub trait Split {
    type Cogrid;
    type Boundaries;
    fn split(self, Self::Boundaries) -> Self::Cogrid;
}

#[derive(Debug, PartialEq, Clone, Abomonation)]
pub struct Grid<T> {
    pub coordinates: Vec<f64>,
    pub values: Vec<T>
}

impl<T, U> Map<Cell, T, U> for Grid<T> {
    type Output = Grid<U>;

    fn map<F: Fn(Cell, T) -> U>(self, f: F) -> Grid<U> {
        let coordinates = self.coordinates;
        let values = cells(&coordinates).zip(self.values)
            .map(|(cell, value)| f(cell, value))
            .collect();
        Grid { coordinates, values }
    }
}

impl<T, U: Clone> Merge<Cogrid<U>> for Grid<T> {
    type Merger = Grid<(T, [U; 2])>;

    fn merge(self, cogrid: Cogrid<U>) -> Grid<(T, [U; 2])> {
        let coordinates = self.coordinates;
        assert_eq!(coordinates, cogrid.coordinates);
        let values = self.values.into_iter()
            .zip(cogrid.values.iter().cloned().zip(cogrid.values.iter().skip(1).cloned()))
            .map(|(value, (left, right))| (value, [left, right]))
            .collect();
        Grid { coordinates, values }
    }
}

impl<T: Clone> Split for Grid<T> {
    type Cogrid = Cogrid<(T, T)>;
    type Boundaries = Boundaries<T>;

    fn split(self, boundaries: Boundaries<T>) -> Cogrid<(T, T)> {
        let coordinates = self.coordinates;
        let values = once(&boundaries.left).chain(&self.values)
            .zip(self.values.iter().chain(once(&boundaries.right)))
            .map(|(a, b)| (a.clone(), b.clone()))
            .collect();
        Cogrid { coordinates, values }
    }
}

impl<T> Grid<T> {
    pub fn from_fns<C: Fn(usize) -> f64, F: Fn(Cell) -> T>(size: usize, coordinate_fn: C, value_fn: F) -> Grid<T> {
        let coordinates: Vec<_> = (0..size + 1).map(coordinate_fn).collect();
        let values = cells(&coordinates).map(value_fn).collect();
        Grid { coordinates, values }
    }

    pub fn periodic_boundaries(&self) -> Boundaries<T>
        where T: Clone
    {
        Boundaries {
            left: self.values[self.values.len() - 1].clone(),
            right: self.values[0].clone()
        }
    }
}

impl<T: 'static> IntoIterator for Grid<T> {
    type Item = (Cell, T);
    type IntoIter = Box<Iterator<Item=Self::Item>>;

    fn into_iter(self) -> Self::IntoIter {
        let cells: Vec<_> = cells(&self.coordinates).collect();
        Box::new(cells.into_iter().zip(self.values))
    }
}

#[derive(Debug, PartialEq, Clone, Abomonation)]
pub struct Boundaries<T> {
    pub left: T,
    pub right: T
}

#[derive(Debug, PartialEq, Clone, Abomonation)]
pub struct Cogrid<T> {
    pub coordinates: Vec<f64>,
    pub values: Vec<T>
}

impl<T, U> Map<(Cell, Cell), T, U> for Cogrid<T> {
    type Output = Cogrid<U>;

    fn map<F: Fn((Cell, Cell), T) -> U>(self, f: F) -> Cogrid<U> {
        let coordinates = self.coordinates;
        let cells: Vec<_> = cells(&coordinates).collect();
        let domain_width = coordinates[coordinates.len() - 1] - coordinates[0];
        let left_ghost = cells[cells.len() - 1].translate(-domain_width);
        let right_ghost = cells[0].translate(domain_width);
        let values = once(&left_ghost).chain(&cells)
            .zip(cells.iter().chain(once(&right_ghost)))
            .zip(self.values)
            .map(|((l, r), value)| f((l.clone(), r.clone()), value))
            .collect();
        Cogrid { coordinates, values }
    }
}

fn cells<'a>(coordinates: &'a [f64]) -> Box<Iterator<Item=Cell> + 'a> {
    Box::new(coordinates.iter().skip(1).zip(coordinates)
        .map(|(&right, &left)| Cell { center: (left + right) / 2., left, right }))
}

#[derive(Clone, Copy, Debug, PartialEq, Abomonation)]
pub struct Cell {
    pub center: f64,
    pub left: f64,
    pub right: f64
}

impl Cell {
    fn translate(self, offset: f64) -> Cell {
        Cell {
            center: self.center + offset,
            left: self.left + offset,
            right: self.right + offset
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn initializes_coordinates_at_cell_edges() {
        assert_eq!(
            Grid::from_fns(2, |x| x as f64, |x| x).coordinates,
            [0., 1., 2.]
        );
    }

    #[test]
    fn initializes_values_with_cell_structs() {
        assert_eq!(
            Grid::from_fns(2, |x| (x as f64).powi(2), |cell| cell.center).values,
            [0.5, 2.5]
        );
    }

    #[test]
    fn maps_values_with_cell_structs() {
        assert_eq!(
            Grid::from_fns(2, |x| x as f64, |x| x).map(|cell, _| cell.left + cell.right).values,
            [1., 3.]
        );
    }

    #[test]
    fn differently_sized_grids_cannot_be_equal() {
        assert_ne!(
            Grid::from_fns(2, |x| x as f64, |_| 0.),
            Grid::from_fns(4, |x| x as f64, |_| 0.)
        );
    }

    #[test]
    fn grids_with_different_values_cannot_be_equal() {
        assert_ne!(
            Grid::from_fns(2, |x| x as f64, |_| [0., 1.]),
            Grid::from_fns(2, |x| x as f64, |_| [1., -3.])
        );
    }

    #[test]
    fn grids_with_different_coordinates_cannot_be_equal() {
        assert_ne!(
            Grid::from_fns(4, |x| x as f64, |_| 0.),
            Grid::from_fns(4, |x| x as f64 * 2., |_| 0.)
        );
    }

    #[test]
    fn cell_translates_correctly() {
        assert_eq!(
            Cell { center: 1., left: 2., right: 3. }.translate(-1.),
            Cell { center: 0., left: 1., right: 2. }
        );
    }

    #[test]
    fn splits_grid_into_cogrid() {
        let boundaries = Boundaries { left: 7., right: 9. };
        assert_eq!(
            Grid::from_fns(2, |x| x as f64, |x| x.center).split(boundaries).values,
            [(7., 0.5), (0.5, 1.5), (1.5, 9.)]
        );
    }

    #[test]
    fn cogrid_has_same_coordinates_as_grid() {
        let grid = Grid::from_fns(2, |x| x as f64, |x| x.center);
        let boundaries = Boundaries { left: 7., right: 9. };
        assert_eq!(
            grid.clone().split(boundaries).coordinates,
            grid.coordinates
        );
    }

    #[test]
    fn cogrid_maps_values_with_adjacent_cells() {
        let boundaries = Boundaries { left: 7., right: 9. };
        let cogrid = Grid::from_fns(2, |x| x as f64, |x| x.center).split(boundaries);
        assert_eq!(
            cogrid.map(|(xa, _), _| xa.center).values,
            [-0.5, 0.5, 1.5]
        );
    }

    #[test]
    fn merges_grid_and_cogrid() {
        let grid = Grid::from_fns(2, |x| x as f64, |x| x.center);
        let boundaries = Boundaries { left: 7., right: 9. };
        let cogrid = grid.clone().split(boundaries).map(|_, (ya, yb)| yb - ya);
        assert_eq!(
            grid.merge(cogrid).values,
            [(0.5, [-6.5, 1.]), (1.5, [1., 7.5])]
        );
    }
}
