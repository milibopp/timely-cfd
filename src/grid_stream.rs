use timely::{ExchangeData, Data};
use timely::dataflow::{Stream, Scope};
use timely::dataflow::operators::{Map, ToStream, Concat, Accumulate, Exchange};
use std::iter::once;
use grid::{Cell, Grid, Cogrid, Boundaries, Map as Map_, Merge, Split};
use dataflow::{Either, zip, tagged_concat};

#[derive(Clone)]
pub struct DistributedGrid<S: Scope, T> {
    stream: Stream<S, Subgrid<T>>
}

impl<S: Scope, T: Data, U: Data> Map_<Cell, T, U> for DistributedGrid<S, T> {
    type Output = DistributedGrid<S, U>;

    fn map<F: Fn(Cell, T) -> U + Clone + 'static>(self, f: F) -> DistributedGrid<S, U> {
        DistributedGrid {
            stream: self.stream.map(move |subgrid| Subgrid {
                grid: subgrid.grid.map(f.clone()),
                peers: subgrid.peers,
                index: subgrid.index
            })
        }
    }
}

impl<S: Scope, T: ExchangeData> Split for DistributedGrid<S, T> {
    type Cogrid = DistributedCogrid<S, (T, T)>;
    type Boundaries = DistributedBoundaries<S, T>;

    fn split(self, boundaries: DistributedBoundaries<S, T>) -> DistributedCogrid<S, (T, T)> {
        let stream = tagged_concat(&self.stream, &self.ghosts(boundaries))
            .accumulate((None, None, None),
                |slots, events| for event in events.drain(..) {
                    collect_either_event(slots, event);
                })
            .map(|(subgrid, left, right)|
                subgrid.expect("split failed to find subgrid").grid.split(
                    Boundaries {
                        left: left.expect("split failed to find left boundary"),
                        right: right.expect("split failed to find right boundary")
                    }));
        DistributedCogrid { stream }
    }
}

impl<S: Scope, T: Data, U: Data> Merge<DistributedCogrid<S, U>> for DistributedGrid<S, T> {
    type Merger = DistributedGrid<S, (T, [U; 2])>;

    fn merge(self, cogrid: DistributedCogrid<S, U>) -> DistributedGrid<S, (T, [U; 2])> {
        let stream = zip(&self.stream, &cogrid.stream)
            .map(|(Subgrid { grid, index, peers }, cogrid)|
                Subgrid { grid: grid.merge(cogrid), index, peers });
        DistributedGrid { stream }
    }
}

impl<S: Scope, T: Data> DistributedGrid<S, T> {
    pub fn from_fns<C, F>(scope: &mut S, number_of_cells: usize, coordinate_fn: C,
                          value_fn: F) -> DistributedGrid<S, T>
        where C: Fn(usize) -> f64,
              F: Fn(Cell) -> T
    {
        let (worker_index, peers) = (scope.index(), scope.peers());
        let start = number_of_cells * worker_index / peers;
        let end = number_of_cells * (worker_index + 1) / peers;
        let subgrid = Subgrid {
            grid: Grid::from_fns(end - start, |local| coordinate_fn(start + local), value_fn),
            index: worker_index as u64,
            peers: peers as u64
        };
        DistributedGrid { stream: once(subgrid).to_stream(scope) }
    }

    pub fn from_stream(stream: Stream<S, Subgrid<T>>) -> Self {
        DistributedGrid { stream }
    }

    pub fn to_stream(self) -> Stream<S, Subgrid<T>> {
        self.stream
    }

    fn ghosts(&self, boundaries: DistributedBoundaries<S, T>) -> Stream<S, (Direction, T)>
        where T: ExchangeData
    {
        self.stream
            .flat_map(|subgrid| subgrid.ghosts())
            .exchange(|&(ref target, _, _)| *target)
            .map(|(_, direction, ghost)| (direction, ghost))
            .concat(&boundaries.stream)
    }

    pub fn periodic_boundaries(&self) -> DistributedBoundaries<S, T>
        where T: ExchangeData
    {
        let stream = self.stream
            .flat_map(move |subgrid| {
                let mut local_boundaries = vec![];
                if subgrid.index == subgrid.peers - 1 {
                    local_boundaries.push((0, Direction::Left, subgrid.last()));
                }
                if subgrid.index == 0 {
                    local_boundaries.push((subgrid.peers - 1, Direction::Right, subgrid.first()));
                }
                local_boundaries
            })
            .exchange(|&(ref target, _, _)| *target)
            .map(|(_, direction, ghost)| (direction, ghost));
        DistributedBoundaries { stream }
    }
}

#[derive(Abomonation, Clone, Copy, Debug, PartialEq)]
pub enum Direction {
    Left,
    Right
}

fn collect_either_event<T>(slots: &mut (Option<Subgrid<T>>, Option<T>, Option<T>), event: Either<Subgrid<T>, (Direction, T)>) {
    match event {
        Either::Left(subgrid) => slots.0 = Some(subgrid),
        Either::Right((Direction::Left, ghost)) => slots.1 = Some(ghost),
        Either::Right((Direction::Right, ghost)) => slots.2 = Some(ghost)
    }
}

#[derive(Clone, Abomonation)]
pub struct Subgrid<T> {
    pub grid: Grid<T>,
    pub index: u64,
    pub peers: u64
}

impl<T: Clone> Subgrid<T> {
    fn ghosts(&self) -> Vec<(u64, Direction, T)> {
        let mut ghosts = vec![];
        if self.index + 1 < self.peers {
            ghosts.push((self.index + 1, Direction::Left, self.last()));
        }
        if self.index > 0 {
            ghosts.push((self.index - 1, Direction::Right, self.first()));
        }
        ghosts
    }

    fn first(&self) -> T {
        self.grid.values[0].clone()
    }

    fn last(&self) -> T {
        self.grid.values[self.grid.values.len() - 1].clone()
    }
}

#[derive(Clone)]
pub struct DistributedBoundaries<S: Scope, T> {
    stream: Stream<S, (Direction, T)>,
}

impl<S: Scope, T: Data> DistributedBoundaries<S, T> {
    pub fn from_values(scope: &mut S, left: T, right: T) -> DistributedBoundaries<S, T> {
        use self::Direction::{Left, Right};
        let boundary = if scope.index() == 0 {
            Some((Left, left))
        } else if scope.index() + 1 == scope.peers() {
            Some((Right, right))
        } else {
            None
        };
        DistributedBoundaries { stream: boundary.to_stream(scope) }
    }

    pub fn to_stream(self) -> Stream<S, (Direction, T)> {
        self.stream
    }
}

pub struct DistributedCogrid<S: Scope, T> {
    stream: Stream<S, Cogrid<T>>
}

impl<S: Scope, T: Data, U: Data> Map_<(Cell, Cell), T, U> for DistributedCogrid<S, T> {
    type Output = DistributedCogrid<S, U>;

    fn map<F: Fn((Cell, Cell), T) -> U + Clone + 'static>(self, f: F) -> DistributedCogrid<S, U> {
        let stream = self.stream.map(move |cogrid| cogrid.map(f.clone()));
        DistributedCogrid { stream }
    }
}

impl<S: Scope, T: Data> DistributedCogrid<S, T> {
    pub fn to_stream(self) -> Stream<S, Cogrid<T>> {
        self.stream
    }
}

#[cfg(test)]
mod tests {
    use timely_test_helper::capture;
    use super::*;

    #[test]
    fn initializes_coordinates_at_cell_edges() {
        let grids = &capture(1, |scope| {
            DistributedGrid::from_fns(scope, 2, |i| i as f64, |x| x.center).to_stream()
        })[0];
        assert_eq!(grids[0].grid.coordinates, [0., 1., 2.]);
    }

    #[test]
    fn distributes_coordinates_across_workers() {
        let grids = &capture(2, |scope| {
            DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center).to_stream()
        })[0];
        assert_eq!(grids[0].grid.coordinates, [0., 1., 2.]);
        assert_eq!(grids[1].grid.coordinates, [2., 3., 4.]);
    }

    #[test]
    fn initializes_values_with_cell_structs() {
        let grids = &capture(2, |scope| {
            DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| [x.left, x.center - 1.]).to_stream()
        })[0];
        assert_eq!(grids[1].grid.values, [[2., 1.5], [3., 2.5]]);
    }

    #[test]
    fn maps_values_with_cell_structs() {
        let grids = &capture(2, |scope| {
            DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center)
                .map(|cell, value| cell.left + value)
                .to_stream()
        })[0];
        assert_eq!(grids[1].grid.values, [4.5, 6.5]);
    }

    #[test]
    fn split_sends_data_across_partition_to_the_right() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .to_stream()
        })[0];
        assert_eq!(cogrids[1].values[0], (1.5, 2.5));
    }

    #[test]
    fn split_sends_data_across_partition_to_the_left() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].values[2], (1.5, 2.5));
    }

    #[test]
    fn split_includes_provided_left_boundary() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].values[0], (3.5, 0.5));
    }

    #[test]
    fn cogrid_has_same_coordinates_as_grid() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].coordinates, [0., 1., 2.]);
    }

    #[test]
    fn periodic_boundaries_for_single_worker() {
        let boundaries = &capture(1, |scope| {
            DistributedGrid::from_fns(scope, 5, |i| i as f64, |x| x.center)
                .periodic_boundaries()
                .to_stream()
        })[0];
        assert_eq!(boundaries, &[(Direction::Left, 4.5), (Direction::Right, 0.5)]);
    }

    #[test]
    fn periodic_boundaries_are_distributed_to_relevant_workers() {
        let boundaries = &capture(3, |scope| {
            DistributedGrid::from_fns(scope, 5, |i| i as f64, |x| x.center)
                .periodic_boundaries()
                .to_stream()
        })[0];
        assert_eq!(boundaries, &[(Direction::Left, 4.5), (Direction::Right, 0.5)]);
    }

    #[test]
    fn custom_boundaries_work_with_split() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            grid.split(DistributedBoundaries::from_values(scope, 7., 9.))
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].values[0], (7., 0.5));
    }

    #[test]
    fn cogrid_maps_values() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .map(|_, (left, right)| left - right)
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].values, [3., -1., -1.]);
    }

    #[test]
    fn cogrid_maps_with_cells() {
        let cogrids = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            grid.split(boundaries)
                .map(|(left, right), _| left.center - right.center)
                .to_stream()
        })[0];
        assert_eq!(cogrids[0].values, [-1., -1., -1.]);
    }

    #[test]
    fn merge_grid_with_cogrid() {
        let merger = &capture(2, |scope| {
            let grid = DistributedGrid::from_fns(scope, 4, |i| i as f64, |x| x.center);
            let boundaries = grid.periodic_boundaries();
            let cogrid = grid.clone().split(boundaries).map(|(left, _), _| left.center);
            grid.merge(cogrid)
                .to_stream()
        })[0];
        assert_eq!(merger[0].grid.values[1], (1.5, [0.5, 1.5]));
    }
}
