extern crate timely;
extern crate timely_cfd;
extern crate nalgebra;

use timely::dataflow::operators::*;
use timely_cfd::dataflow::{scan, boundary_stepper_1d};
use timely_cfd::geometry::Grid;
use timely_cfd::output::{write_msgpack, filename};
use nalgebra::Vector2;

fn main() {
    timely::execute_from_args(std::env::args(), |root| {
        let number_of_cells = 1_000;
        let index = root.index();
        let initial = Grid::from_fn(number_of_cells, root.index(), root.peers(), discontinuity);
        let grid_spacing = initial.spacing();
        root.dataflow(|scope| {
            let state = scan(scope, (0, initial), move |state| {
                boundary_stepper_1d(state, {
                    use timely_cfd::physics::schemes;
                    use timely_cfd::physics::riemann::hll;
                    use timely_cfd::physics::fluid::Isothermal1DFluid;

                    let timestep = 0.2 * grid_spacing;
                    move |grid, boundaries|
                        schemes::godunov(
                            timestep, grid, [boundaries.left, boundaries.right],
                            |left, right| hll(Isothermal1DFluid { sound_speed: 1.0 }, *left, *right))
                })
                    .filter(|&(timestep, _)| timestep > 1_000)
            });
            state.inspect(move |&(timestep, ref grid)|
                write_msgpack(filename(index, timestep), grid)
                    .expect("failed to write output file!"));
        });
    })
        .expect("timely computation failed");
}

fn discontinuity(x: f64) -> Vector2<f64> {
    Vector2::new(if x < 0.5 { 1.0 } else { 0.5 }, 0.0)
}
