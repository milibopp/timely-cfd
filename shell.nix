with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "timely-cfd";
  buildInputs = [ rustChannels.nightly.rust ];
}
