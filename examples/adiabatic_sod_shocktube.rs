extern crate timely;
extern crate timely_cfd;
extern crate nalgebra;

use timely::dataflow::operators::*;
use timely_cfd::dataflow::{scan, boundary_stepper_1d};
use timely_cfd::geometry::{Boundaries, Grid};
use timely_cfd::output::{write_msgpack, filename};
use nalgebra::Vector3;


fn main() {
    timely::execute_from_args(std::env::args(), |root| {
        let number_of_cells = 1_000;
        let number_of_timesteps = 1_000;
        let index = root.index();
        let initial = Grid::from_fn(number_of_cells, root.index(), root.peers(), discontinuity);
        root.dataflow(|scope| {
            let state = scan(scope, (0, initial), move |state| {
                boundary_stepper_1d(state, step)
                    .filter(move |&(timestep, _)| timestep <= number_of_timesteps)
            });
            state.inspect(move |&(timestep, ref grid)|
                write_msgpack(filename(index, timestep), grid)
                    .expect("failed to write output file!"));
        });
    })
        .expect("timely computation failed");
}

fn step(grid: Grid<Vector3<f64>>, boundaries: Boundaries<[Vector3<f64>; 2]>) -> Grid<Vector3<f64>> {
    use timely_cfd::physics::schemes::muscl_hancock;
    use timely_cfd::physics::slope_limiter;
    use timely_cfd::physics::riemann::hll;
    use timely_cfd::physics::fluid::Adiabatic1DFluid;

    let timestep = 0.2 * grid.spacing();
    let fluid = Adiabatic1DFluid { adiabatic_index: 7. / 5. };
    muscl_hancock(
        timestep, grid, [boundaries.left, boundaries.right],
        move |left, right| hll(fluid, *left, *right), fluid, slope_limiter::superbee
    )
}

fn discontinuity(x: f64) -> Vector3<f64> {
    if x <= 0.5 {
        Vector3::new(1.0, 0.0, 1.0)
    } else {
        Vector3::new(0.125, 0.0, 0.1)
    }
}
