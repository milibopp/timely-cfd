pub use self::isothermal::Isothermal1DFluid;
pub use self::adiabatic::Adiabatic1DFluid;

mod isothermal;
mod adiabatic;

pub trait HyperbolicEquation<Quantity> {
    type Flux;

    fn flux(&self, quantity: &Quantity) -> Self::Flux;
}

pub trait SignalSpeed<Quantity> {
    type Speed;

    fn left_speed(&self, quantity: &Quantity) -> Self::Speed;
    fn right_speed(&self, quantity: &Quantity) -> Self::Speed;
}
