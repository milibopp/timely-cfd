use geometry::Grid;
use alga::linear::{FiniteDimVectorSpace, VectorSpace};
use std::iter::once;
use ::physics::fluid::HyperbolicEquation;
use ::physics::slope_limiter::multi_component;


/// First-order upwind Godunov scheme
pub fn godunov<R, V>(timestep: f64, grid: Grid<V>, boundaries: [V; 2], riemann_solver: R) -> Grid<V>
    where R: Fn(&V, &V) -> V,
          V: VectorSpace<Field = f64> + Copy
{
    let fluxes = fluxes(&grid, &boundaries, riemann_solver);
    step_with_fluxes(timestep, grid, &fluxes)
}

fn fluxes<R, V>(grid: &Grid<V>, boundaries: &[V; 2], riemann_solver: R) -> Vec<V>
    where R: Fn(&V, &V) -> V
{
    grid.pairs(boundaries)
        .map(|pair| riemann_solver(pair[0], pair[1]))
        .collect()
}

pub mod alt {
    use grid::{Cell, Map, Merge, Split};
    use alga::linear::VectorSpace;

    pub fn godunov<G, C, R, V>(timestep: f64, grid: G, boundaries: G::Boundaries, riemann_solver: R) -> G
        where R: Fn(&V, &V) -> V + 'static + Clone,
              V: VectorSpace<Field = f64> + Copy,
              G: Clone + Split + Merge<C>,
              G::Cogrid: Map<(Cell, Cell), (V, V), V, Output=C>,
              G::Merger: Map<Cell, (V, [V; 2]), V, Output=G>
    {
        step_with_fluxes(timestep, grid.clone(), fluxes(&grid, boundaries, riemann_solver))
    }

    fn fluxes<G, C, R, V: Clone>(grid: &G, boundaries: G::Boundaries, riemann_solver: R) -> C
        where R: Fn(&V, &V) -> V + 'static + Clone,
              G: Clone + Split + Merge<C>,
              G::Cogrid: Map<(Cell, Cell), (V, V), V, Output=C>,
    {
        grid.clone().split(boundaries)
            .map(move |_, (ya, yb)| riemann_solver(&ya, &yb))
    }

    fn step_with_fluxes<G, C, V>(timestep: f64, grid: G, fluxes: C) -> G
        where V: VectorSpace<Field=f64> + Copy,
              G: Merge<C>,
              G::Merger: Map<Cell, (V, [V; 2]), V, Output=G>
    {
        grid.merge(fluxes)
            .map(move |cell, (quantity, fluxes)| quantity + (fluxes[1] - fluxes[0]) * (timestep / (cell.right - cell.left)))
    }
}

/// MUSCL-Hancock scheme
pub fn muscl_hancock<R, V, E, L>(timestep: f64, grid: Grid<V>, boundaries: [[V; 2]; 2],
                                 riemann_solver: R, equation: E, slope_limiter: L) -> Grid<V>
    where R: Fn(&V, &V) -> V,
          V: FiniteDimVectorSpace<Field = f64> + Copy,
          E: HyperbolicEquation<V, Flux = V>,
          L: Fn([&f64; 3]) -> f64
{
    let fluxes = {
        let slopes = grid.wide_stencil(&boundaries)
            .map(|stencil| multi_component(&slope_limiter, stencil));
        let extended = once(&boundaries[0][1]).chain(grid.cells()).chain(once(&boundaries[1][0]));
        let reconstruction = extended.zip(slopes)
            .map(|(quantity, slope)| (*quantity - slope * 0.5, *quantity + slope * 0.5));

        let evolved: Vec<_> = reconstruction
            .map(|(left, right)| {
                let influx = (equation.flux(&left) - equation.flux(&right))
                    * (0.5 * timestep / grid.spacing());
                (left + influx, right + influx)
            })
            .collect();

        evolved.iter().zip(evolved.iter().skip(1))
            .map(|(&(_, left), &(right, _))| riemann_solver(&left, &right))
            .collect()
    };

    step_with_fluxes(timestep, grid, &fluxes)
}

fn step_with_fluxes<V>(timestep: f64, mut grid: Grid<V>, fluxes: &Vec<V>) -> Grid<V>
    where V: VectorSpace<Field=f64> + Copy
{
    let grid_spacing = grid.spacing();
    for ((left_flux, right_flux), cell) in fluxes.iter()
        .zip(fluxes.iter().skip(1))
        .zip(grid.cells_mut())
    {
        *cell = *cell + (*left_flux - *right_flux) * (timestep / grid_spacing);
    }
    grid
}
