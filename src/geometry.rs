use std::iter::once;

#[derive(Clone, Abomonation, Debug, Serialize)]
pub struct Grid<Cell> {
    data: Vec<Cell>,
    index: usize,
    number_of_domains: usize,
    number_of_cells: usize
}

impl<Cell> Grid<Cell> {
    pub fn from_fn<Initializer>(number_of_cells: usize, index: usize,
                                number_of_domains: usize, function: Initializer)
                                -> Grid<Cell>
        where Initializer: Fn(f64) -> Cell
    {
        let start = index * number_of_cells / number_of_domains;
        let end = (index + 1) * number_of_cells / number_of_domains;
        let coordinate = |cell_index| cell_index as f64 / number_of_cells as f64;
        Grid {
            data: (start..end).map(coordinate).map(function).collect(),
            index, number_of_domains, number_of_cells
        }
    }

    pub fn spacing(&self) -> f64 {
        1. / self.number_of_cells as f64
    }

    pub fn left_boundary(&self) -> Cell where Cell: Clone {
        self.data[0].clone()
    }

    pub fn right_boundary(&self) -> Cell where Cell: Clone {
        self.data[self.data.len() - 1].clone()
    }

    fn wide_left_boundary(&self) -> [Cell; 2] where Cell: Clone {
        [self.data[0].clone(), self.data[1].clone()]
    }

    fn wide_right_boundary(&self) -> [Cell; 2] where Cell: Clone {
        [self.data[self.data.len() - 2].clone(), self.data[self.data.len() - 1].clone()]
    }

    pub fn pairs<'a>(&'a self, boundaries: &'a [Cell; 2]) -> Box<Iterator<Item=[&'a Cell; 2]> + 'a> {
        Box::new(
            once([&boundaries[0], &self.data[0]])
                .chain(self.data.iter().zip(self.data.iter().skip(1)).map(|(a, b)| [a, b]))
                .chain(once([&self.data[self.data.len() - 1], &boundaries[1]]))
        )
    }

    pub fn stencil<'a>(&'a self, boundaries: &'a [Cell; 2]) -> Box<Iterator<Item=[&'a Cell; 3]> + 'a> {
        Box::new(
            once(&boundaries[0]).chain(&self.data)
                .zip(&self.data)
                .zip(self.data.iter().skip(1).chain(once(&boundaries[1])))
                .map(|((a, b), c)| [a, b, c])
        )
    }

    pub fn wide_stencil<'a>(&'a self, boundaries: &'a [[Cell; 2]; 2]) -> Box<Iterator<Item=[&'a Cell; 3]> + 'a> {
        let extended_grid = || boundaries[0].iter().chain(&self.data).chain(&boundaries[1]);
        Box::new(
            extended_grid().zip(extended_grid().skip(1)).zip(extended_grid().skip(2))
                .map(|((a, b), c)| [a, b, c])
        )
    }

    pub fn cells<'a>(&'a self) -> Box<Iterator<Item=&'a Cell> + 'a> {
        Box::new(self.data.iter())
    }

    pub fn cells_mut<'a>(&'a mut self) -> Box<Iterator<Item=&'a mut Cell> + 'a> {
        Box::new(self.data.iter_mut())
    }

    pub fn cells_with_coordinates<'a>(&'a self) -> Box<Iterator<Item=(f64, &'a Cell)> + 'a> {
        let number_of_cells = self.data.len();
        Box::new(
            (0..number_of_cells)
                .map(move |i| i as f64 / number_of_cells as f64)
                .zip(&self.data)
        )
    }

    pub fn step<F>(&self, boundaries: &[Cell; 2], function: F) -> Grid<Cell>
        where F: Fn([&Cell; 3]) -> Cell
    {
        Grid {
            data: self.stencil(boundaries).map(function).collect(),
            .. *self
        }
    }
}

pub trait Bounded<B> {
    fn boundaries(&self) -> Vec<(usize, Direction, B)>;
}

impl<T: Clone> Bounded<T> for Grid<T> {
    fn boundaries(&self) -> Vec<(usize, Direction, T)> {
        let left = (previous_index(self.index, self.number_of_domains), Direction::Left, self.left_boundary());
        let right = (next_index(self.index, self.number_of_domains), Direction::Right, self.right_boundary());
        vec!(left, right)
    }
}

impl<T: Clone> Bounded<[T; 2]> for Grid<T> {
    fn boundaries(&self) -> Vec<(usize, Direction, [T; 2])> {
        let left = (previous_index(self.index, self.number_of_domains), Direction::Left, self.wide_left_boundary());
        let right = (next_index(self.index, self.number_of_domains), Direction::Right, self.wide_right_boundary());
        vec!(left, right)
    }
}

#[derive(Abomonation, Clone)]
pub struct Boundaries<T> {
    pub left: T,
    pub right: T
}

impl<T> Boundaries<T> {
    pub fn collect<I: IntoIterator<Item=(Direction, T)>>(tagged: I) -> Boundaries<T> {
        let mut storage = Boundaries { left: None, right: None };
        for (tag, boundary) in tagged {
            match tag {
                Direction::Left => storage.right = Some(boundary),
                Direction::Right => storage.left = Some(boundary)
            }
        }
        Boundaries {
            left: storage.left.unwrap(),
            right: storage.right.unwrap()
        }
    }
}


#[derive(Copy, Clone, PartialEq, Eq, Debug, Abomonation)]
pub enum Direction { Left, Right }

fn next_index(index: usize, total: usize) -> usize {
    if index + 1 < total { index + 1 } else { 0 }
}

fn previous_index(index: usize, total: usize) -> usize {
    if index > 0 { index - 1 } else { total - 1 }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn boundary_iterator_tags_left_value() {
        let (left, here) = (1, 2);
        let grid = Grid::from_fn(20, here, 4, |x| x);
        assert!(Bounded::<f64>::boundaries(&grid).into_iter()
            .any(|(tag, _, value)| tag == left && value == 0.5));
    }

    #[test]
    fn boundary_iterator_tags_right_value() {
        let (here, right) = (2, 3);
        let grid = Grid::from_fn(20, here, 4, |x| x);
        assert!(Bounded::<f64>::boundaries(&grid).into_iter()
            .any(|(tag, _, value)| tag == right && value == 0.7));
    }

    #[test]
    fn boundary_iterator_provides_direction() {
        let grid = Grid::from_fn(20, 2, 4, |x| x);
        assert_eq!(Bounded::<f64>::boundaries(&grid)[0].1, Direction::Left);
    }

    #[test]
    fn provided_function_is_applied_to_coordinate() {
        assert_eq!(Grid::from_fn(10, 0, 1, |_| 2.23).left_boundary(), 2.23);
    }

    #[test]
    fn grid_only_contains_local_subdomain() {
        let number_of_domains = 2;
        let index = 0;
        let expected_value_at_right_end_of_domain = 0.45;
        assert_eq!(
            Grid::from_fn(20, index, number_of_domains, |x| x)
                .right_boundary(),
            expected_value_at_right_end_of_domain
        );
    }

    #[test]
    fn stencil_contains_left_boundary_first() {
        let boundaries = [2.3, 4.7];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(
            grid.stencil(&boundaries).next(),
            Some([&boundaries[0], &0.0, &0.1])
        );
    }

    #[test]
    fn stencil_iterates_over_inner_pairs() {
        let boundaries = [2.3, 4.7];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(
            grid.stencil(&boundaries).skip(2).next(),
            Some([&0.1, &0.2, &0.3])
        );
    }

    #[test]
    fn stencil_contains_right_boundary_last() {
        let boundaries = [2.3, 4.7];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(
            grid.stencil(&boundaries).last(),
            Some([&0.8, &0.9, &4.7])
        );
    }

    #[test]
    fn stencil_is_as_long_as_the_subdomain() {
        let boundaries = [2.3, 4.6];
        let grid_length = 10;
        let grid = Grid::from_fn(grid_length, 0, 1, |x| x);
        assert_eq!(grid.stencil(&boundaries).count(), grid_length);
    }

    #[test]
    fn wide_stencil_contains_left_boundary_in_first_stencil() {
        let boundaries = [[2.2, 2.3], [4.4, 4.5]];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.wide_stencil(&boundaries).next(), Some([&2.2, &2.3, &0.0]));
    }

    #[test]
    fn wide_stencil_contains_left_boundary_in_second_stencil() {
        let boundaries = [[2.2, 2.3], [4.4, 4.5]];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.wide_stencil(&boundaries).nth(1), Some([&2.3, &0.0, &0.1]));
    }

    #[test]
    fn wide_stencil_contains_right_boundary_at_the_end() {
        let boundaries = [[2.2, 2.3], [4.4, 4.5]];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.wide_stencil(&boundaries).last(), Some([&0.9, &4.4, &4.5]));
    }

    #[test]
    fn first_pair_contains_left_boundary() {
        let boundaries = [2.3, 4.6];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.pairs(&boundaries).next().unwrap()[0], &boundaries[0]);
    }

    #[test]
    fn first_pair_contains_first_cell() {
        let boundaries = [2.3, 4.6];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.pairs(&boundaries).next().unwrap()[1], grid.cells().next().unwrap());
    }

    #[test]
    fn last_pair_contains_right_boundary() {
        let boundaries = [2.3, 4.6];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.pairs(&boundaries).last().unwrap()[1], &boundaries[1]);
    }

    #[test]
    fn last_pair_contains_last_cell() {
        let boundaries = [2.3, 4.6];
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(grid.pairs(&boundaries).last().unwrap()[0], grid.cells().last().unwrap());
    }

    #[test]
    fn middle_pair_contains_correct_cells() {
        let grid = Grid::from_fn(10, 0, 1, |x| x);
        assert_eq!(
            grid.pairs(&[2., 5.]).nth(2).unwrap(),
            [&grid.data[1], &grid.data[2]]
        );
    }

    #[test]
    fn next_index_is_one_higher() {
        assert_eq!(next_index(1, 4), 2);
        assert_eq!(next_index(3, 8), 4);
    }

    #[test]
    fn next_index_wrap_around_at_the_end() {
        assert_eq!(next_index(3, 4), 0);
    }

    #[test]
    fn previous_index_is_one_lower() {
        assert_eq!(previous_index(2, 4), 1);
        assert_eq!(previous_index(3, 6), 2);
    }

    #[test]
    fn previous_index_wraps_around_at_the_beginning() {
        assert_eq!(previous_index(0, 3), 2);
    }

    #[test]
    fn grid_with_vectorial_data() {
        let grid = Grid::from_fn(10, 0, 1, |x| [x, x / 2.0]);
        assert_eq!(grid.left_boundary(), [0.0, 0.0]);
    }

    #[test]
    fn iterates_over_grid_cells_with_coordinates() {
        let grid = Grid::from_fn(10, 0, 1, |x| x + 1.0);
        assert_eq!(grid.cells_with_coordinates().next(), Some((0.0, &1.0)));
    }
}
