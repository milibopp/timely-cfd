extern crate timely_cfd;

use timely_cfd::physics::riemann::{RiemannProblem, Primitive};
use timely_cfd::physics::fluid::Adiabatic1DFluid;

fn main() {
    let args = std::env::args().skip(1).map(|s| s.parse().unwrap()).collect::<Vec<f64>>();
    assert!(args.len() == 7);
    let shocktube = RiemannProblem {
        left: Primitive { density: args[0], pressure: args[1], velocity: args[2] },
        right: Primitive { density: args[3], pressure: args[4], velocity: args[5] },
        fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
    };
    let n = 10_000;
    let width = 1.0;
    for x in (0..n).map(|i| width * i as f64 / n as f64 - width / 2.) {
        let q = shocktube.solve(args[6], x);
        println!("{} {} {} {}", x, q[0], q[1], q[2]);
    }
}
