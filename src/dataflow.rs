use std;
use std::iter::once;
use timely::{Data, ExchangeData};
use timely::dataflow::operators::*;
use timely::dataflow::stream::Stream;
use timely::dataflow::scopes::{Child, Scope, ScopeParent};
use geometry::{Grid, Bounded, Boundaries};

pub fn scan<'a, T, S, G>(scope: &mut Child<'a, G, u64>, initial: T, stepper: S) -> Stream<Child<'a, G, u64>, T>
    where G: ScopeParent,
          S: for<'b> FnOnce(&Stream<Child<'b, G, u64>, T>) -> Stream<Child<'b, G, u64>, T> + 'static,
          T: Data
{
    let (helper, cycle) = scope.loop_variable(std::u64::MAX, 1);
    let state = once(initial).to_stream(scope).concat(&cycle);
    stepper(&state).connect_loop(helper);
    state
}

pub fn stencil_stepper<T, S, G>(stencil: S, state: &Stream<G, (u64, Grid<T>)>) -> Stream<G, (u64, Grid<T>)>
    where T: ExchangeData,
          G: Scope,
          S: Fn([&T; 3]) -> T + 'static
{
    boundary_stepper_1d(state, move |grid, boundaries| grid.step(&[boundaries.left, boundaries.right], &stencil))
}

pub fn boundary_stepper_1d<T, B, F, G>(state: &Stream<G, (u64, Grid<T>)>, function: F) -> Stream<G, (u64, Grid<T>)>
    where T: ExchangeData,
          B: ExchangeData,
          G: Scope,
          Grid<T>: Bounded<B>,
          F: Fn(Grid<T>, Boundaries<B>) -> Grid<T> + 'static
{
    let grid = state.map(|(_, grid)| grid);
    zip(&periodic_boundaries(&grid), &state)
        .map(move |(boundaries, (timestep, grid))|
             (timestep + 1, function(grid, boundaries)))
}

fn periodic_boundaries<T: ExchangeData, B: ExchangeData, G: Scope>(state: &Stream<G, Grid<T>>) -> Stream<G, Boundaries<B>>
    where Grid<T>: Bounded<B>
{
    state
        .flat_map(|grid| grid.boundaries())
        .exchange(|&(index, _, _)| index as u64)
        .accumulate(Vec::new(), |container, data|
            for (_, tag, value) in data.drain(..) {
                container.push((tag, value));
            }
        )
        .map(Boundaries::collect)
}

#[derive(Clone, Abomonation, Debug)]
pub enum Either<A, B> {
    Left(A),
    Right(B)
}

/// Zips two streams that contain exactly one item per timestamp.
pub fn zip<G: Scope, A: Data, B: Data>(left: &Stream<G, A>, right: &Stream<G, B>)
    -> Stream<G, (A, B)>
{
    use self::Either::*;
    tagged_concat(left, right)
        .accumulate((None, None),
            |container, data| for data in data.drain(..) {
                match data {
                    Left(value) => container.0 = Some(value),
                    Right(value) => container.1 = Some(value)
                }
            }
        )
        .map(|(a, b)| (a.unwrap(), b.unwrap()))
}

pub fn tagged_concat<G: Scope, A: Data, B: Data>(left: &Stream<G, A>, right: &Stream<G, B>)
    -> Stream<G, Either<A, B>>
{
    left.map(Either::Left)
        .concat(&right.map(Either::Right))
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::assert_that;
    use spectral::numeric::FloatAssertions;
    use timely;
    use std::iter::once;

    #[test]
    #[should_panic]
    fn observes_output_of_scan() {
        timely::example(|scope| {
            scan(scope, 0, |state| state.map(|x| x))
                .inspect(|_| panic!("this is supposed to happen"));
        });
    }

    #[test]
    fn scan_does_not_include_failed_element() {
        timely::example(|scope| {
            scan(scope, 0, |state| state.map(|i| i + 1).filter(|&i| i < 10))
                .inspect(|&i| assert!(i < 10));
        });
    }

    #[test]
    #[should_panic]
    fn scan_includes_initial_element() {
        timely::example(|scope| {
            scan(scope, 0, |state| state.map(|i| i + 1).filter(|&i| i < 10))
                .inspect(|&i| assert!(i > 0));
        });
    }

    #[test]
    fn scan_does_not_clone() {
        #[derive(Abomonation)]
        struct Unclonable;
        impl Clone for Unclonable {
            fn clone(&self) -> Unclonable { unimplemented!() }
        }

        timely::example(|scope| {
            scan(scope, (0, Unclonable),
                |state| state
                    .map(|(i, x)| (i + 1, x))
                    .inspect(|_| ())
                    .filter(|&(i, _)| i > 10)
            );
        });
    }

    #[test]
    fn periodic_boundaries_for_single_thread() {
        timely::example(|scope| {
            let grid = Grid::from_fn(10, scope.index(), scope.peers(), |x| x);
            let stream = once(grid.clone()).to_stream(scope);
            periodic_boundaries(&stream).inspect(move |boundaries| {
                assert_that!(boundaries.right).is_equal_to(grid.left_boundary());
                assert_that!(boundaries.left).is_equal_to(grid.right_boundary());
            });
        });
    }

    #[test]
    fn periodic_boundaries_for_multiple_threads() {
        use timely::Configuration::*;
        timely::execute(Process(2), |worker|
            worker.dataflow::<(),_,_>(|scope| {
                let grid = Grid::from_fn(10, scope.index(), scope.peers(), |x| x);
                let stream = once(grid.clone()).to_stream(scope);
                periodic_boundaries(&stream).inspect(move |boundaries| {
                    let last = grid.right_boundary();
                    let first = grid.left_boundary();
                    assert_that!(boundaries.right).is_close_to((last + 0.1) % 1.0, 0.001);
                    assert_that!(boundaries.left).is_close_to((first + 0.9) % 1.0, 0.001);
                });
            })
        ).unwrap();
    }
}
