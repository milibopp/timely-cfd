use nalgebra::{Vector3, zero};
use super::{HyperbolicEquation, SignalSpeed};

#[derive(Clone, Copy)]
pub struct Adiabatic1DFluid {
    pub adiabatic_index: f64
}

impl HyperbolicEquation<Vector3<f64>> for Adiabatic1DFluid {
    type Flux = Vector3<f64>;

    fn flux(&self, quantity: &Vector3<f64>) -> Vector3<f64> {
        let (density, momentum, energy) = (quantity[0], quantity[1], quantity[2]);
        if density > 0.0 {
            Vector3::new(
                momentum,
                momentum.powi(2) / density + self.pressure(quantity),
                momentum / density * (energy + self.pressure(quantity))
            )
        } else {
            zero()
        }
    }
}

impl Adiabatic1DFluid {
    pub fn from_primitive(&self, density: f64, pressure: f64, velocity: f64) -> Vector3<f64> {
        Vector3::new(
            density,
            density * velocity,
            density * velocity.powi(2) / 2. + pressure / (self.adiabatic_index - 1.)
        )
    }

    pub fn pressure(&self, quantity: &Vector3<f64>) -> f64 {
        let (density, momentum, energy) = (quantity[0], quantity[1], quantity[2]);
        (self.adiabatic_index - 1.) * (energy - momentum.powi(2) / (density * 2.))
    }

    pub fn sound_speed(&self, quantity: &Vector3<f64>) -> f64 {
        (self.adiabatic_index * self.pressure(quantity) / quantity[0])
            .sqrt()
    }
}

impl SignalSpeed<Vector3<f64>> for Adiabatic1DFluid {
    type Speed = f64;

    fn left_speed(&self, quantity: &Vector3<f64>) -> f64 {
        self.speed(quantity) - self.sound_speed(quantity)
    }

    fn right_speed(&self, quantity: &Vector3<f64>) -> f64 {
        self.speed(quantity) + self.sound_speed(quantity)
    }
}

impl Adiabatic1DFluid {
    fn speed(&self, quantity: &Vector3<f64>) -> f64 {
        if quantity[0] > 0.0 {
            quantity[1] / quantity[0]
        } else {
            0.0
        }
    }
}
