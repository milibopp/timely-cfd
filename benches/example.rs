extern crate timely_cfd;
extern crate easybench;
extern crate nalgebra;

use easybench::*;
use timely_cfd::geometry::Grid;
use timely_cfd::physics::riemann::hll;
use timely_cfd::physics::godunov;
use timely_cfd::physics::fluid::Adiabatic1DFluid;
use nalgebra::Vector3;

fn main() {
    let number_of_cells = 100;
    let initial_grid = Grid::from_fn(number_of_cells, 0, 1, |x| Vector3::new(x, x, 0.0));
    let fluid = Adiabatic1DFluid { adiabatic_index: 7. / 5. };
    let grid_spacing = 1.0 / number_of_cells as f64;
    let timestep = 0.2 * grid_spacing;
    println!(
        "hll_100_cells {}",
        bench_env(Some(initial_grid), move |grid| {
            let grid = grid.take().unwrap();
            let b = Vector3::new(0.5, 0.5, 0.0);
            godunov::step(timestep, grid_spacing, grid, [b, b], |left, right| hll(fluid, *left, *right))
        })
    );
}
