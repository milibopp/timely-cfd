#! /usr/bin/env nix-shell
#! nix-shell ../env/analysis.nix -i python

import os
import matplotlib.pyplot as plt
import numpy

dirname = 'output/riemann_problem/'
simulations = {
    name: numpy.genfromtxt(dirname + name)
    for name in os.listdir(dirname)
}

fig, axes = plt.subplots(nrows=3, sharex=True, figsize=(8, 12))

for i in range(3):
    for name, data in simulations.items():
        axes[i].plot(data[:, 0], data[:, i + 1], label=name)
    plt.legend()
plt.savefig('plots/riemann_problem.png')
