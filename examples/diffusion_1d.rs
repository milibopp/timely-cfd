extern crate timely;
extern crate timely_cfd;

use timely::dataflow::operators::*;
use timely_cfd::dataflow::{scan, stencil_stepper};
use timely_cfd::geometry::Grid;
use timely_cfd::output::{write_msgpack, filename};
use timely_cfd::physics;

fn main() {
    timely::execute_from_args(std::env::args(), |root| {
        let number_of_cells = 100;
        let index = root.index();
        let initial = Grid::from_fn(number_of_cells, root.index(), root.peers(), gauss_initial);
        let grid_spacing = initial.spacing();
        root.dataflow(|scope| {
            let state = scan(scope, (0, initial), move |state| {
                stencil_stepper({
                    let timestep = 0.4 * grid_spacing.powi(2);
                    move |stencil: [&f64; 3]|
                        physics::diffusion(timestep, grid_spacing, stencil)
                }, state)
                    .filter(|&(timestep, _)| timestep > 1_000)
            });
            state.inspect(move |&(timestep, ref grid)| {
                if timestep % 20 == 0 {
                    write_msgpack(filename(index, timestep), grid)
                        .expect("failed to write output file!")
                }
            });
        });
    })
        .expect("timely computation failed");
}

fn gauss_initial(x: f64) -> f64 {
    (-((x - 0.5) / 0.1).powi(2)).exp()
}
