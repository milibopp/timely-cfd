use std::ops::Div;
use ::physics::fluid::{HyperbolicEquation, SignalSpeed, Adiabatic1DFluid};
use alga::linear::FiniteDimVectorSpace;

pub fn hll<E, V>(equation: E, left: V, right: V) -> V
    where E: HyperbolicEquation<V, Flux=V> + SignalSpeed<V, Speed=f64>,
          V: FiniteDimVectorSpace<Field = f64> + Div<f64, Output = V> + Copy
{
    let average = roe_average(left, right);
    let left_speed = equation.left_speed(&average);
    let right_speed = equation.right_speed(&average);
    if left_speed >= 0.0 {
        equation.flux(&left)
    } else if right_speed < 0.0 {
        equation.flux(&right)
    } else {
        (equation.flux(&left) * right_speed - equation.flux(&right) * left_speed
            + (right - left) * (left_speed * right_speed))
            / (right_speed - left_speed)
    }
}

fn roe_average<V>(left: V, right: V) -> V
    where V: FiniteDimVectorSpace<Field = f64> + Div<f64, Output = V> + Copy,
{
    let left_weight = left[0].sqrt();
    let right_weight = right[0].sqrt();
    let normalization = 1. / (left_weight + right_weight);
    left * left_weight * normalization + right * right_weight * normalization
}

#[derive(Copy, Clone)]
pub struct Primitive {
    pub density: f64,
    pub velocity: f64,
    pub pressure: f64
}

impl Primitive {
    fn mirror(self) -> Primitive {
        Primitive { velocity: -self.velocity, .. self }
    }
}

#[derive(Copy, Clone)]
pub struct RiemannProblem {
    pub left: Primitive,
    pub right: Primitive,
    pub fluid: Adiabatic1DFluid
}

fn sound_speed(gamma: f64, pressure: f64, density: f64) -> f64 {
    (gamma * pressure / density).sqrt()
}

pub fn star_region_pressure_root(star_region_pressure: f64,
                             left: Primitive, right: Primitive, gamma: f64) -> f64
{
    (right.velocity - left.velocity)
        + adaptive_root(star_region_pressure, gamma, left)
        + adaptive_root(star_region_pressure, gamma, right)
}

fn star_region_pressure_root_derivative(
    star_region_pressure: f64, left: Primitive, right: Primitive, gamma: f64) -> f64
{
    adaptive_root_derivative(star_region_pressure, gamma, left)
        + adaptive_root_derivative(star_region_pressure, gamma, right)
}

fn adaptive_root(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    if star_region_pressure > state.pressure {
        shock_root(star_region_pressure, gamma, state)
    } else {
        rarefaction_root(star_region_pressure, gamma, state)
    }
}

fn adaptive_root_derivative(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    if star_region_pressure > state.pressure {
        shock_root_derivative(star_region_pressure, gamma, state)
    } else {
        rarefaction_root_derivative(star_region_pressure, gamma, state)
    }
}

fn rarefaction_root(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    let pressure_ratio = star_region_pressure / state.pressure;
    let sound_speed = sound_speed(gamma, state.pressure, state.density);
    2. * sound_speed / (gamma - 1.)
        * (pressure_ratio.powf((gamma - 1.) / (2. * gamma)) - 1.)
}

fn rarefaction_root_derivative(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    let pressure_ratio = star_region_pressure / state.pressure;
    let sound_speed = sound_speed(gamma, state.pressure, state.density);
    1. / (sound_speed * state.density)
        * pressure_ratio.powf(-(gamma + 1.) / (2. * gamma))
}

fn shock_root(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    let p = star_region_pressure;
    let pk = state.pressure;
    let rho = state.density;
    let a = 2. / ((gamma + 1.) * rho);
    let b = pk * (gamma - 1.) / (gamma + 1.);

    (a / (p + b)).sqrt() * (p - pk)
}

fn shock_root_derivative(star_region_pressure: f64, gamma: f64, state: Primitive) -> f64 {
    let p = star_region_pressure;
    let pk = state.pressure;
    let rho = state.density;
    let a = 2. / ((gamma + 1.) * rho);
    let b = pk * (gamma - 1.) / (gamma + 1.);

    (a / (p + b)).sqrt()
        * (1. - (p - pk) / (2. * (p + b)))
}

fn star_region_pressure(left: Primitive, right: Primitive, gamma: f64) -> f64 {
    use roots::{find_root_newton_raphson, SimpleConvergency};
    find_root_newton_raphson(
        initial_guess(left, right, gamma),
        |pressure| star_region_pressure_root(pressure, left, right, gamma),
        |pressure| star_region_pressure_root_derivative(pressure, left, right, gamma),
        &mut SimpleConvergency { eps: 1e-6f64, max_iter: 100 }
    ).unwrap()
}

fn initial_guess(left: Primitive, right: Primitive, gamma: f64) -> f64 {
    let cl = sound_speed(gamma, left.pressure, left.density);
    let cr = sound_speed(gamma, right.pressure, right.density);
    let exponent = (gamma - 1.) / (2. * gamma);
    (
        (cl + cr - 0.5 * (gamma - 1.) * (right.velocity - left.velocity))
        / (cl / left.pressure.powf(exponent) + cr / right.pressure.powf(exponent))
    )
        .powf(1. / exponent)
}

fn left_shock(gamma: f64, left: Primitive, star_region_pressure: f64, star_region_velocity: f64, velocity: f64) -> Primitive {
    let pressure_ratio = star_region_pressure / left.pressure;
    let shock_speed = left.velocity - sound_speed(gamma, left.pressure, left.density)
        * ((gamma + 1.) / (2. * gamma) * pressure_ratio + (gamma - 1.) / (2. * gamma)).sqrt();
    if velocity <= shock_speed {
        left
    } else {
        let factor = (gamma - 1.) / (gamma + 1.);
        let density = left.density * (pressure_ratio + factor) / (pressure_ratio * factor + 1.);
        Primitive { density, pressure: star_region_pressure, velocity: star_region_velocity }
    }
}

fn left_rarefaction(gamma: f64, left: Primitive, star_region_pressure: f64, star_region_velocity: f64, velocity: f64) -> Primitive {
    let pressure_ratio = star_region_pressure / left.pressure;
    let sound_speed_left = sound_speed(gamma, left.pressure, left.density);
    let sound_speed_behind = sound_speed_left * pressure_ratio.powf((gamma - 1.) / (2. * gamma));
    let head_velocity = left.velocity - sound_speed_left;
    let tail_velocity = star_region_velocity - sound_speed_behind;
    if velocity <= head_velocity {
        left
    } else if velocity <= tail_velocity {
        let a = 2. / (gamma + 1.);
        let b = (gamma - 1.) / (gamma + 1.);
        let c = 2. / (gamma - 1.);
        let term = a + b / sound_speed_left * (left.velocity - velocity);
        Primitive {
            density: left.density * term.powf(c),
            velocity: a * (sound_speed_left + left.velocity / c + velocity),
            pressure: left.pressure * term.powf(c * gamma)
        }
    } else {
        Primitive {
            density: left.density * pressure_ratio.powf(1. / gamma),
            pressure: star_region_pressure,
            velocity: star_region_velocity
        }
    }
}

fn sample_by_velocity(left: Primitive, right: Primitive, gamma: f64, velocity: f64) -> Primitive {
    let star_region_pressure = star_region_pressure(left, right, gamma);
    let star_region_velocity = (left.velocity + right.velocity
            + adaptive_root(star_region_pressure, gamma, right)
            - adaptive_root(star_region_pressure, gamma, left)
        ) / 2.;
    if velocity <= star_region_velocity {
        if star_region_pressure > left.pressure {
            left_shock(gamma, left, star_region_pressure, star_region_velocity, velocity)
        } else {
            left_rarefaction(gamma, left, star_region_pressure, star_region_velocity, velocity)
        }
    } else {
        if star_region_pressure > right.pressure {
            left_shock(gamma, right.mirror(), star_region_pressure, -star_region_velocity, -velocity).mirror()
        } else {
            left_rarefaction(gamma, right.mirror(), star_region_pressure, -star_region_velocity, -velocity).mirror()
        }
    }
}


impl RiemannProblem {
    pub fn solve(&self, time: f64, location: f64) -> [f64; 3] {
        let x = sample_by_velocity(self.left, self.right, self.fluid.adiabatic_index, location / time);
        [x.density, x.pressure, x.velocity]
    }
}

#[cfg(test)]
mod shocktube_tests {
    use super::*;

    #[test]
    fn initial_left_state_preserved_at_time_zero() {
        let shocktube = some_shocktube();
        assert_eq!(
            shocktube.solve(0.0, -0.5),
            [shocktube.left.density, shocktube.left.pressure, 0.0]
        );
    }

    #[test]
    fn initial_right_state_preserved_at_time_zero() {
        let shocktube = some_shocktube();
        assert_eq!(shocktube.solve(0.0, 0.5), [0.2, 0.3, 0.0]);
    }

    fn some_shocktube() -> RiemannProblem {
        RiemannProblem {
            left: Primitive { density: 1.0, pressure: 0.8, velocity: 0.0 },
            right: Primitive { density: 0.2, pressure: 0.3, velocity: 0.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        }
    }

    #[test]
    fn left_mirrored_states_is_preserved() {
        let mirrored = some_mirrored_shocktube();
        assert_eq!(
            mirrored.solve(0.0, -0.5),
            [mirrored.left.density, mirrored.left.pressure, mirrored.left.velocity]
        );
    }

    #[test]
    fn right_mirrored_states_is_preserved() {
        let mirrored = some_mirrored_shocktube();
        assert_eq!(
            mirrored.solve(0.0, 0.5),
            [mirrored.right.density, mirrored.right.pressure, mirrored.right.velocity]
        );
    }

    fn some_mirrored_shocktube() -> RiemannProblem {
        RiemannProblem {
            left: Primitive { density: 0.3, pressure: 0.5, velocity: 1.0 },
            right: Primitive { density: 1.0, pressure: 1.0, velocity: -0.5 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        }
    }

    // Note for future reference: in the following tests, literature values from “Riemann Solvers
    // and Numerical Methods for Fluid Dynamics“ by Eleuterio Toro (3. edition) are used. They can
    // be found in Table 4.3 on page 131 of the book.

    #[derive(Copy, Clone)]
    struct StarRegion { pressure: f64, velocity: f64, left_density: f64, right_density: f64 }

    fn validate_star_region(problem: RiemannProblem, expected: StarRegion) {
        let delta = if expected.velocity >= 0. { 0.01 } else { -0.01 };
        let left = problem.solve(1.0, expected.velocity * (1. - delta));
        let right = problem.solve(1.0, expected.velocity * (1. + delta));
        let tolerance = 5e-3;
        assert_relative_eq!(left[2], expected.velocity, max_relative = tolerance);
        assert_relative_eq!(left[1], expected.pressure, max_relative = tolerance);
        assert_relative_eq!(left[0], expected.left_density, max_relative = tolerance);
        assert_relative_eq!(right[0], expected.right_density, max_relative = tolerance);
    }


    #[test]
    fn sod_shocktube() {
        let problem = RiemannProblem {
            left: Primitive { density: 1.0, pressure: 1.0, velocity: 0.0 },
            right: Primitive { density: 0.125, pressure: 0.1, velocity: 0.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 0.30313,
            velocity: 0.92745,
            left_density: 0.42632,
            right_density: 0.26557
        };
        validate_star_region(problem, literature);
    }

    #[test]
    fn mirrored_shocktube() {
        let problem = RiemannProblem {
            left: Primitive { density: 0.125, pressure: 0.1, velocity: 0.0 },
            right: Primitive { density: 1.0, pressure: 1.0, velocity: 0.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 0.30313,
            velocity: -0.92745,
            left_density: 0.26557,
            right_density: 0.42632
        };
        validate_star_region(problem, literature);
    }

    #[test]
    fn star_region_pressure_in_123_problem() {
        let left = Primitive { density: 1.0, pressure: 0.4, velocity: -2.0 };
        let right = Primitive { density: 1.0, pressure: 0.4, velocity: 2.0 };
        assert_relative_eq!(star_region_pressure(left, right, 1.4), 0.00189, epsilon = 1e-4);
    }

    #[test]
    fn the_123_problem() {
        let problem = RiemannProblem {
            left: Primitive { density: 1.0, pressure: 0.4, velocity: -2.0 },
            right: Primitive { density: 1.0, pressure: 0.4, velocity: 2.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 0.00189,
            velocity: 0.0,
            left_density: 0.02185,
            right_density: 0.02185
        };
        validate_star_region(problem, literature);
    }

    #[test]
    fn woodward_collela_left_half() {
        let problem = RiemannProblem {
            left: Primitive { density: 1.0, pressure: 1000.0, velocity: 0.0 },
            right: Primitive { density: 1.0, pressure: 0.01, velocity: 0.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 460.894,
            velocity: 19.5975,
            left_density: 0.57506,
            right_density: 5.99924
        };
        validate_star_region(problem, literature);
    }

    #[test]
    fn woodward_collela_right_half() {
        let problem = RiemannProblem {
            left: Primitive { density: 1.0, pressure: 0.01, velocity: 0.0 },
            right: Primitive { density: 1.0, pressure: 100.0, velocity: 0.0 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 46.0950,
            velocity: -6.19633,
            left_density: 5.99242,
            right_density: 0.57511
        };
        validate_star_region(problem, literature);
    }

    #[test]
    fn woodward_collela_collision() {
        let problem = RiemannProblem {
            left: Primitive { density: 5.99924, pressure: 460.894, velocity: 19.5975 },
            right: Primitive { density: 5.99242, pressure: 46.0950, velocity: -6.19633 },
            fluid: Adiabatic1DFluid { adiabatic_index: 7. / 5. }
        };
        let literature = StarRegion {
            pressure: 1691.64,
            velocity: 8.68975,
            left_density: 14.2823,
            right_density: 31.0426
        };
        validate_star_region(problem, literature);
    }
}
