with import <nixpkgs> {};

let vispy = import ./vispy.nix;

in stdenv.mkDerivation rec {
  name = "data-analysis";

  buildInputs = [
    (pkgs.python36.withPackages (p: with p; [
      matplotlib
      numpy
      msgpack
    ]))
  ];
}
